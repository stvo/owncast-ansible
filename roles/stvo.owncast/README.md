# Ansible Playbook for a owncast host

This is an ansible role for setting up an owncast server.
The role is based on the installation instructions of https://owncast.online/quickstart/manual/, https://owncast.online/docs/sslproxies/, https://owncast.online/docs/systemservice/ and is not using docker.



I put this together in about 30 mins and tested it once. So please don't expect it to be perfect :)
